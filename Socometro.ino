#include <BluetoothSerial.h>
#include <LiquidCrystal_I2C.h>
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>

// Criação das Handlers das tasks para manipulação das mesmas por nomes genéricos

TaskHandle_t Task1;
TaskHandle_t Task2;
TaskHandle_t Task3;
TaskHandle_t Task4;
TaskHandle_t Task5;


Adafruit_MPU6050 mpu;
LiquidCrystal_I2C display(0x27, 16, 2);
BluetoothSerial BT;

// Inicialização de variáveis

uint32_t timeNow; // variável de armazenamento de tempo para o modo TdR
float soma = 0;
bool flag_punch; // flag que indica se houve soco
bool BTconnection = false; 
bool flag_ready = false; // flag de tempo fixo para medições do acelerômetro
float Pontuacao = 0;
float MAX = 0;
int Comando;
int evento = 1;

// Set de pinos

int LedVermelho = 33;
int LedAmarelo = 26;
int LedVerde = 27;
int Buzzer = 25;

// Estados e Eventos da Máquina de Estados

enum estados { DESCONECTADO, ESPERA_JOGO, CONTAGEM, CONTAGEM_ALEATORIA, ESPERA_SOCO, EXIBICAO };
enum eventos { BLUETOOTH, WAIT, DESCONECTAR, MODO_FORCA, MODO_REACAO, FINAL, TERM_SOCO, REINICIA };

// Estado inicial

int estado = DESCONECTADO;

struct transicao{
  int estadonovo;
};

struct transicao matriz_transicao[6][8];

// Função para conexão Bluetooth

void callback(esp_spp_cb_event_t event, esp_spp_cb_param_t*param){
  
  if(event == ESP_SPP_SRV_OPEN_EVT){
    Serial.println("Client Connected");
    BTconnection = true;
  }

  if(event == ESP_SPP_CLOSE_EVT ){
    BTconnection = false;
    Serial.println("Client Disconnected");
  }  
}

// Inicialização da Máquina de Estados e suas transições

void InitMaqEstados() {
    // A00
    matriz_transicao[DESCONECTADO][BLUETOOTH].estadonovo = ESPERA_JOGO; 
    
    // A01
    matriz_transicao[ESPERA_JOGO][DESCONECTAR].estadonovo = DESCONECTADO;
    
    // A02
    matriz_transicao[ESPERA_JOGO][WAIT].estadonovo = ESPERA_JOGO;
    
    // A03
    matriz_transicao[ESPERA_JOGO][MODO_FORCA].estadonovo = CONTAGEM;

    // A04
    matriz_transicao[ESPERA_JOGO][MODO_REACAO].estadonovo = CONTAGEM_ALEATORIA;

    // A05
    matriz_transicao[CONTAGEM_ALEATORIA][FINAL].estadonovo = ESPERA_SOCO;

    // A05
    matriz_transicao[CONTAGEM][FINAL].estadonovo = ESPERA_SOCO;

    // A06
    matriz_transicao[ESPERA_SOCO][TERM_SOCO].estadonovo = EXIBICAO;

    // A07
    matriz_transicao[EXIBICAO][REINICIA].estadonovo = ESPERA_JOGO;   
}

// Task de leitura do canal Bluetooth 

void TaskBTread(void *pvParameters){
  const TickType_t xDelay50ms = pdMS_TO_TICKS(50);
  for (;;){
    if (BT.available()>0){
      Comando = BT.read();
      Serial.print(Comando);
    }
  vTaskDelay(xDelay50ms);

  }
}

// Task de processamento da Máquina de Estados

void TaskDisplay(void *pvParameters){
  const TickType_t xDelay50ms = pdMS_TO_TICKS(50);
  for(;;){
    mpu.setAccelerometerRange(MPU6050_RANGE_16_G);
    estado = matriz_transicao[estado][evento].estadonovo; // Atualiza Estado a cada loop
    if (estado == DESCONECTADO){
      MAX = 0;
      display.clear();
      xTaskCreatePinnedToCore(TaskAguardConLed, "TaskAguardConLed", 10000, NULL, 10, &Task3, 0); // Cria Task paralela de sequência de LEDs de aguardo
      while(!BTconnection){ // Loop de aguardo da conexão Bluetooth
        display.setCursor(0,0);
        display.print("Aguard. Conex.");
        delay(100);
      }
      display.clear();
      display.setCursor(0,0);
      display.print("Conectado!");
      digitalWrite(Buzzer, HIGH);
      delay(250);
      digitalWrite(Buzzer, LOW);
      digitalWrite(LedVerde, LOW); // Reset dos LEDs caso haja conexão
      digitalWrite(LedAmarelo, LOW);
      digitalWrite(LedVermelho, LOW);
      delay(1000);
      evento = BLUETOOTH; // Evento de Bluetooth conectado
      vTaskDelete(Task3); // Delete da Task de sequência de LEDs de aguardo
      display.clear();
    }
    if (estado == ESPERA_JOGO){
      MAX = 0;
      digitalWrite(LedVerde, LOW); // Reset dos LEDs para o evento FINAL (transição A05)
      digitalWrite(LedAmarelo, LOW);
      digitalWrite(LedVermelho, LOW);
      display.setCursor(0,0);
      display.print("Escolha o modo: ");
      display.setCursor(0,1);
      display.print("1. Forca 2. TdR");
      evento = WAIT; // Evento de aguardo para selecionar opção
      if (!BTconnection) { 
        evento = DESCONECTAR; // Evento de desconectar do Bluetooth (retorna à tela inicial)
      }
      delay(100);

      if (Comando == 'F'){ // Se usuário selecionar Modo de força
        evento = MODO_FORCA;
      }
      if (Comando == 'T'){ // Se usuário selecionar Modo de Tempo de Reação
        evento = MODO_REACAO;
      }
    }
    if (estado == CONTAGEM){ // Inicia contagem de LEDs e Buzzer para o Modo força
      display.clear();
      display.setCursor(0,0);
      display.print("<Modo de forca>");
      delay(1000);
      digitalWrite(LedVermelho, HIGH);
      digitalWrite(Buzzer, HIGH);
      delay(250);
      digitalWrite(Buzzer, LOW);
      delay(1250);
      digitalWrite(LedAmarelo, HIGH);
      digitalWrite(Buzzer, HIGH);
      delay(250);
      digitalWrite(Buzzer, LOW);
      delay(1250);
      digitalWrite(LedVerde, HIGH);
      digitalWrite(Buzzer, HIGH);
      evento = FINAL;
    }

    if (estado == CONTAGEM_ALEATORIA){ // Inicia contagem aleatória de LEDs e Buzzer para o modo de Tempo de Reação
      display.clear();
      display.setCursor(0,0);
      display.print("<Modo de reacao>");
      delay(1000);
      digitalWrite(LedVermelho, HIGH);
      digitalWrite(Buzzer, HIGH);
      delay(250);
      digitalWrite(Buzzer, LOW);
      delay(1250);
      digitalWrite(LedAmarelo, HIGH);
      digitalWrite(Buzzer, HIGH);
      delay(250);
      digitalWrite(Buzzer, LOW);
      delay(random(1500,5000)); // Aleatoriedade provida da função random com intervalo de delay
      digitalWrite(LedVerde, HIGH);
      digitalWrite(Buzzer, HIGH);
      evento = FINAL;
    }
    
    if (estado == ESPERA_SOCO){
      if (Comando == 'F'){
        // Task paralela - em core 0 - que demora um tempo fixo antes de resetar a flag flag_ready
        xTaskCreatePinnedToCore(TaskFixedTime, "TaskFixedTime", 10000, NULL, 13, &Task4, 0); 
        flag_ready = true;
        while(flag_ready){ // Loop de medições do acelerômetro, armazena valor máximo em MAX e sai após a TaskFixedTime resetar a flag_ready
          sensors_event_t a, g, temp;
          mpu.getEvent(&a, &g, &temp);
          // Cálculo do módulo das acelerações
          soma = abs(sqrt(a.acceleration.x*a.acceleration.x+a.acceleration.y*a.acceleration.y+a.acceleration.z*a.acceleration.z) - 9.8);
          Serial.print("Acceleration X: ");
          Serial.print(a.acceleration.x);
          Serial.print(", Y: ");
          Serial.print(a.acceleration.y);
          Serial.print(", Z: ");
          Serial.println(a.acceleration.z);
          Serial.print("soma: ");
          Serial.println(soma);
          if (soma > MAX){
            MAX = soma;
            Serial.print("MAX: ");
            Serial.println(MAX);
          }
        }
        display.clear();
        evento = TERM_SOCO;
      }
      if (Comando == 'T'){
       // Task paralela - em core 0 - cuja função é desligar o Buzzer em tempo determinado e sem afetar o tempo obtido no momento do soco
       xTaskCreatePinnedToCore(TaskFixedTime2, "TaskFixedTime2", 10000, NULL, 14, &Task5, 0);
       flag_punch = false;
       // Função retorna tempo em us desde a inicialização do ESP32
       int64_t timeOld = esp_timer_get_time();
       while(!flag_punch){
         sensors_event_t a, g, temp;
         mpu.getEvent(&a, &g, &temp);
         soma = abs(sqrt(a.acceleration.x*a.acceleration.x+a.acceleration.y*a.acceleration.y+a.acceleration.z*a.acceleration.z) - 9.8);
         if(soma > 1){ // Threshold para considerar soco 
          flag_punch = true;
         }
      }
      // Tempo mensurado para detectar o soco
      timeNow = esp_timer_get_time() - timeOld; 
      evento = TERM_SOCO;
      delay(1000);  
      }
   }
   if (estado == EXIBICAO){
    if (Comando == 'F'){
      display.clear();
      display.setCursor(0,0);
      display.print("Pontuacao: ");
      display.setCursor(0,1);
      Pontuacao = MAX;
      // Transforma int em string para exibição no APP
      String strPont = String(Pontuacao*10); 
      display.print(strPont);
      // Envio da string para o APP
      BT.print(strPont);
      delay(10000);
      evento = REINICIA;
      Comando = 0;
    }
    if (Comando == 'T'){
      display.clear();
      display.setCursor(0,0);
      display.print("Tempo: ");
      display.setCursor(0,1);
      // Transforma int em string para exibição no APP e transforma em ms
      String strTime = String(timeNow*0.001);
      display.print(strTime);
      // Envio da string para o APP
      BT.print(strTime);
      delay(15000);
      evento = REINICIA;
      Comando = 0;
    }
   }
    
  }
  vTaskDelay(xDelay50ms);
}

// Task de sequência de LEDs para aguardar conexão
void TaskAguardConLed(void *pvParameters){ 
  for(;;){
    delay(100);
    digitalWrite(LedVerde, HIGH);
    delay(1000);
    digitalWrite(LedAmarelo, HIGH);
    delay(1000);
    digitalWrite(LedVermelho, HIGH);
    delay(1000);
    digitalWrite(LedVermelho, LOW);
    delay(1000);
    digitalWrite(LedAmarelo, LOW);
    delay(1000);
    digitalWrite(LedVerde, LOW);
    delay(1000);
  }

}

// Task de tempo fixo para desligar buzzer e resetar flag
void TaskFixedTime(void *pvParameters){
  for(;;){
    delay(500);
    digitalWrite(Buzzer,LOW);
    delay(5000);
    flag_ready = false;
    vTaskDelete(NULL);
  }
}

// Task de tempo fixo para desligar buzzer
void TaskFixedTime2(void *pvParameters){
  for(;;){
    delay(500);
    digitalWrite(Buzzer,LOW);
    vTaskDelete(NULL);
  }
}


void setup() {
  Serial.begin(9600);
  BT.begin("ESP32");
  BT.register_callback(callback);
  if (!mpu.begin()) {
    Serial.println("Sensor init failed");
    while (1)
      yield();
  }
  // Set de pinos dos LEDs e Buzzer para saída
  pinMode(25, OUTPUT);
  pinMode(26, OUTPUT);
  pinMode(27, OUTPUT);
  pinMode(33, OUTPUT);
  InitMaqEstados();
  display.begin();
  display.backlight();
  display.setCursor(0,0);
  display.print("SIST. EMBARCADOS");
  display.setCursor(0,1);
  display.print("    GRUPO G    ");
  delay(2000);
  display.clear();

  // Criação das tasks principais
  xTaskCreatePinnedToCore(
                          TaskBTread,
                          "TaskBTread",
                          10000,
                          NULL,
                          15,
                          &Task2,
                          0
                          );
  xTaskCreatePinnedToCore(
                          TaskDisplay,
                          "TaskDisplay",
                          10000,
                          NULL,
                          21,
                          &Task1,
                          1
                          );
}

void loop() {
  // Não é necessário 
}
